### A Pluto.jl notebook ###
# v0.19.3

using Markdown
using InteractiveUtils

# ╔═╡ 8390db70-52ce-4801-aa11-994cdcc1097f
begin
	using Pkg
	import Pkg;
	Pkg.add("DataStructures")
	Pkg.activate("Project.toml")
	Pkg.add("PlutoUI")
	using Markdown
	using InteractiveUtils
	using PlutoUI
	using DataStructures
end

# ╔═╡ 2665e958-559d-49ac-b453-486526261582
mutable struct Direction 
    Name::String
    Cost::Int64
end

# ╔═╡ 510c981a-279f-4bcd-ad89-bf48e7b8c34d
MU = Direction("move up", 1)

# ╔═╡ 4f7efa48-906f-4e1d-b7dd-8f7b5122708b
MD = Direction("move down", 1)

# ╔═╡ 0cb082aa-9056-47e7-a222-d9d89fc789cd
ME = Direction("move east", 2)

# ╔═╡ fb25ed89-579e-4b23-8705-ba7b7157a139
MW = Direction("move west", 2)

# ╔═╡ d08f675f-e5cc-4295-afa3-0f89c7c56497
CO = Direction("collect item", 5)

# ╔═╡ ce980930-1cdb-4175-bda5-c68f7466fb7a
TransitionModel= Dict()

# ╔═╡ b97e5089-d891-44f7-a7e8-0a22add49454
mutable struct State
	numberOfStoreys::Int64
	floorNumber::Int64
	officeNumber::Int64
	occupiedOfficesPerFloor::Int64
	parcelsInOffice::Int64
	officeNumber::Int64
	location::Int64
end

# ╔═╡ c10b2e90-8b06-40f0-81a8-7da8c133292e
struct Node{TState, TCost <: Number}
  data::TState
  depth::Int32
  g::TCost
  f::TCost
  parent::Union{Node{TState, TCost}, Nothing}
end

# ╔═╡ 767c772b-6f74-4729-8a6b-64f6f62fd2cd
nodeOrderKey(n::Node) = n.f

# ╔═╡ 4095770e-a4c0-4951-8b55-285e45083ef6
firstHeuristic(state, goal) = three(Int64)

# ╔═╡ 46e56b74-1b4a-43ab-8066-8598371daf53
function astar(begin, state, goal)
	begin = time()
	problemQueue = []
	problem = FormulateProblem(state, goal);
	sequence = Search(problem);
	entered = Set()
	
	while length(problemQueue) !=0
		_, cost, path, presentIn = heappop!(problemQueue_queue)
		if sequence == goal
			return path, push!(entered, presentIn)
		elseif sequence == failure
			return null
		end
		push!(entered, presentIn)
	end
end

# ╔═╡ 461d4f4c-0191-43e7-8399-e84c83d71c74
function bestPath(n::Node)
  outcome = [no.parcelsInOffice]
  
  while !isnothing(n.officeNumber)
    n = n.parent
    push!(results, n.data)
  end
  return reverse!(results)
end

# ╔═╡ 536caa31-20c1-442a-8f70-46aa7485f108
function createResult(transitionModel, predecessor, initialState, goalState)
	outcome = []
	agent = goal_state
	
	while !(agent == initial_state)
		presentInStatePredecessor = predecessor[agent]
		relatedTransitions = transitionModelmodel[presentInStatePredecessor]
		for singlularTransition in relatedTransitions
			if singlularTransition[2] == agent
				push!(outcome, singlularTransition[1])
				break
			else
				continue
			end
		end
		agent = presentInStatePredecessor
	end
	return outcome
end


# ╔═╡ 1584c020-f2fe-44dd-9893-8a11c8344aae
function heuristic(cell,goal)
	while true
 		if office(empty)
 			return failure
		elseif (node==goalstate)
 			return goal
		end
	end
end



# ╔═╡ 0b03037c-ed05-4db1-bf45-2bf75d4bfd52
function isGoal(presentInState, goalState)
	if goalstate(Node.location) then
            if nNode.pos:equals(goalState) then
                closed:push(nNode)
                return true
            end
	end
	return goalState
end

# ╔═╡ 18321b6e-af0a-4e9c-827f-2e37a5488ec6
function goalState(floorNumber,goal)
	return astar
end

# ╔═╡ 2df876f3-05e4-4667-9e8c-aa487a6bcd42
astar(floorNumber,officeNumber);

# ╔═╡ Cell order:
# ╠═8390db70-52ce-4801-aa11-994cdcc1097f
# ╠═2665e958-559d-49ac-b453-486526261582
# ╠═0cb082aa-9056-47e7-a222-d9d89fc789cd
# ╠═fb25ed89-579e-4b23-8705-ba7b7157a139
# ╠═510c981a-279f-4bcd-ad89-bf48e7b8c34d
# ╠═4f7efa48-906f-4e1d-b7dd-8f7b5122708b
# ╠═d08f675f-e5cc-4295-afa3-0f89c7c56497
# ╠═ce980930-1cdb-4175-bda5-c68f7466fb7a
# ╠═b97e5089-d891-44f7-a7e8-0a22add49454
# ╠═c10b2e90-8b06-40f0-81a8-7da8c133292e
# ╠═767c772b-6f74-4729-8a6b-64f6f62fd2cd
# ╠═4095770e-a4c0-4951-8b55-285e45083ef6
# ╠═46e56b74-1b4a-43ab-8066-8598371daf53
# ╠═461d4f4c-0191-43e7-8399-e84c83d71c74
# ╠═536caa31-20c1-442a-8f70-46aa7485f108
# ╠═1584c020-f2fe-44dd-9893-8a11c8344aae
# ╠═0b03037c-ed05-4db1-bf45-2bf75d4bfd52
# ╠═18321b6e-af0a-4e9c-827f-2e37a5488ec6
# ╠═2df876f3-05e4-4667-9e8c-aa487a6bcd42
